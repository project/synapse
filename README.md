# synapse

## Чем занимается?

> Добавляет код ГТМ.
> Вставляет теги для верификации сайта.

## Требования к платформе

- _Drupal 8-11_
- _PHP 7.4.0+_

## Версии

- [Drupal.org prod версия](https://www.drupal.org/project/synapse)

```sh
composer require 'drupal/synapse:^1.10'
```

## Как использовать?

> Форма настроект доступна по ссылке /admin/config/synapse/settings.
> Поля для верификации yandex webmaster, google webmaster.
> Поле для гтм-идентификатора.
> Поле для google-аналитики.
> Чекбокс для отключения gtm в админке.
