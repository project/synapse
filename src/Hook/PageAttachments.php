<?php

namespace Drupal\synapse\Hook;

/**
 * PreprocessHtml.
 */
class PageAttachments {

  /**
   * Hook.
   */
  public static function hook(array &$page) {
    $config = \Drupal::config('synapse.settings');
    self::yandexWebmaster($page, $config);
    self::googleWebmaster($page, $config);
  }

  /**
   * Google webmaster.
   */
  private static function googleWebmaster(&$page, $config) {
    if ($config->get('wm-google')) {
      $page['#attached']['html_head'][] = [
        [
          '#tag' => 'meta',
          '#attributes' => [
            'name'    => 'google-site-verification',
            'content' => $config->get('wm-google'),
          ],
        ],
        'google',
      ];
    }
  }

  /**
   * Yandex webmaster.
   */
  private static function yandexWebmaster(&$page, $config) {
    if ($config->get('wm-yandex')) {
      $page['#attached']['html_head'][] = [
        [
          '#tag' => 'meta',
          '#attributes' => [
            'name'    => 'yandex-verification',
            'content' => $config->get('wm-yandex'),
          ],
        ],
        'yandex',
      ];
    }
  }

}
