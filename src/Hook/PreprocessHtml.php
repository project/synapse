<?php

namespace Drupal\synapse\Hook;

/**
 * PreprocessHtml.
 */
class PreprocessHtml {

  /**
   * Hook.
   */
  public static function hook(array &$variables) {
    $config = \Drupal::config('synapse.settings');
    $path = \Drupal::service('path.current')->getPath();
    $admin = FALSE;
    if (\Drupal::currentUser()->id() == 1 && $config->get('gtm-admin-disable')) {
      $admin = TRUE;
    }
    if ($config->get('gtm-id') && substr($path, 0, 7) != '/admin/' && !$admin) {
      $gtm = $config->get('gtm-id');
      $script = "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','{$gtm}');\n";
      $variables['page_bottom']['gtm'] = [
        '#markup' => "<script type='text/javascript'>{$script}</script>",
        '#allowed_tags' => ['script'],
        '#weight' => 999,
      ];
    }
  }

}
